<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#C72121</headerColor>
        <logo>AppIcon_PTRH</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <description>Roll up information from any object with clicks and not code.</description>
    <formFactors>Large</formFactors>
    <label>Rollup Helper</label>
    <navType>Standard</navType>
    <tab>PS_Rollup_Helper</tab>
    <tab>Rollup_Helper_Help</tab>
    <tab>PS_All_Rollups</tab>
    <uiType>Lightning</uiType>
</CustomApplication>
