/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global abstract class ISAPCalloutService extends skyvvasolutions.IAbstractCalloutService implements skyvvasolutions.GisOdbgIntegration.IProcessOut {
    global ISAPCalloutService() {

    }
    global override void doMap(Map<String,String> mapRecord) {

    }
    global override String integrate(skyvvasolutions__Adapter__c a, Map<String,String> mr, skyvvasolutions__IMessage__c msg) {
        return null;
    }
}
